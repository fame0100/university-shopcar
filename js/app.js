//116. Variables
const carrito = document.querySelector('#carrito');
const contenedorCarrito = document.querySelector('#lista-carrito tbody');
const vaciarCarrito = document.querySelector('#vaciar-carrito');
const listaCursos = document.querySelector('#lista-cursos');
//117. Agregando el arreglo vacio para el carrito
let articulosCarrito = [];

//116. Funcion donde registro todos los eventListeners
cargarEventListeners();

function cargarEventListeners() {
    //116. Cuando agregas un curso presionando agregar
    listaCursos.addEventListener('click', agregarCurso);

    //120. Elimina cursos del carrito
    carrito.addEventListener('click', eliminarCurso);

    //121. Vaciar el carrito
    vaciarCarrito.addEventListener('click', () => {
        articulosCarrito = []; //121. Reseteamos el arreglo

        limpiarHTML(); //121. Eliminamos todo el HTML
    });
}


//116. Funciones
function agregarCurso(e) {
    e.preventDefault();

    if (e.target.classList.contains('agregar-carrito')) { //116. Contains nos checa si contiene esa clase, entonces solo va a seleccionar el boton para evitar hacer bubbling
        // console.log(e.target.parentElement.parentElement);
        const cursoSeleccionado = e.target.parentElement.parentElement;
        leerDatosCurso(cursoSeleccionado); //Agregandole el valor de la variable cursoSeleccionado, mandarle llamar essa funcion
    }
}

//120. Elimina un curso del carrito
function eliminarCurso(e) {
    //console.log(e.target.classList);
    if (e.target.classList.contains('borrar-curso')) {
        //console.log(e.target.getAttribute('data-id'));
        const cursoId = e.target.getAttribute('data-id');

        //120. Elimina del arreglo usando de articulosCarrito por el data-id
        articulosCarrito = articulosCarrito.filter(curso => curso.id !== cursoId);

        console.log(articulosCarrito);
        carritoHTML(); //120. Iterar sobre el carrito y mostrar su HTML

    }
}

//116. Leer los datos del curso(lee el contenido del html al que le dimos click y extrae la informacion del curso)
function leerDatosCurso(curso) {
    // console.log(curso);

    //116. Crear un objeto con el contenido del curso actual
    const infoCurso = {
            imagen: curso.querySelector('img').src, //116. En este caso se selecciona curso que es la seccion que se esta seleccionado
            titulo: curso.querySelector('h4').textContent,
            precio: curso.querySelector('.precio span').textContent,
            id: curso.querySelector('a').getAttribute('data-id'), //116. seleccionar atributos de un enlace
            cantidad: 1
        }
        // console.log(infoCurso);

    //119. Revisa si un elemento ya existe en el carrito
    const existe = articulosCarrito.some(curso => curso.id === infoCurso.id);
    // va a iterar por cada elemento de curso, si alguno es igual al que estamos tratando de actualizar, se actualiza al carrito y no se agrega
    if (existe) {
        //119. Actualizamos la cantidad
        const cursos = articulosCarrito.map(curso => {
            if (curso.id === infoCurso.id) {
                curso.cantidad++;
                return curso; //119. Retorna el objeto actualizado
            } else {
                return curso; //119. Retorna los objetos que no son los duplicados
            }
        });
        articulosCarrito = [...cursos]; //119. Agregar el nuevo arreglo
    } else {
        //119. Agregamos el curso al carrito
        //117. Agrega elementos al arreglo del carrito, usando spred operator
        articulosCarrito = [...articulosCarrito, infoCurso]; //117. Vamos a tomar una copia y le vamos a ir agregando el objeto de infoCurso
    }



    console.log(articulosCarrito);
    //117. Mandar a llamar la funcion despues de llamar los datos del curso y agregarlos al carrito
    carritoHTML();
}


//117. Muestra el carrito de compras en el html
function carritoHTML() {

    //117. Limpiar el html
    limpiarHTML();

    //117. Recorre el carrito y genera un html  
    articulosCarrito.forEach((curso) => {
        //118. Nos da la informacion del objeto actual
        // console.log(curso);

        // 118. Utilizar destructuring
        const { imagen, titulo, precio, cantidad, id } = curso; //118. Extrae el valor y nos crea la variable

        const row = document.createElement('tr'); //117. Se crea un nuevo table row
        row.innerHTML = `
        <td>
             <img src="${imagen}" width="100">
        </td>            
        <td> ${titulo} </td>
        <td> ${precio} </td>
        <td> ${cantidad} </td>
        <td>
            <a href="#" class="borrar-curso" data-id="${id}"> X </a>
        </td>
        `;
        //117. Se manda a llamar curso.titulo de infoCurso


        //117. Agrega el html del carrito en el tbody
        contenedorCarrito.appendChild(row);
    });
}

//117. ELimina los cursos del tbody
function limpiarHTML() {
    //117. Forma lenta
    //contenedorCarrito.innerHTML = '';

    //117. Forma performance
    while (contenedorCarrito.firstChild) {
        contenedorCarrito.removeChild(contenedorCarrito.firstChild);
    }
}